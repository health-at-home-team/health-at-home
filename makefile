# modified version of Collatz makefile
.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

ifeq ($(shell uname -s), Darwin)
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage3
    MYPY          := mypy
    PYDOC         := pydoc3
    PYLINT        := pylint
    PYTHON        := python3
else ifeq ($(shell uname -p), unknown)
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage
    MYPY          := mypy
    PYDOC         := pydoc
    PYLINT        := pylint
    PYTHON        := python
else
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage3
    MYPY          := mypy
    PYDOC         := pydoc3
    PYLINT        := pylint3
    PYTHON        := python3
endif

docker-compose:
	docker-compose up -d --force-recreate

# get git config
config:
	git config -l

# get git log
show-log:
	git log

all:
	make check
	make test
	make run

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml

# check the existence of check files
check: $(CFILES)
	@echo "MAKE CHECK (PARTIAL IMPLEMENTATION?): OK"

# remove temporary files
clean:
	rm -f  .coverage
	rm -f  .pylintrc
	rm -f  *.pyc
	rm -f  *.tmp
	rm -rf __pycache__
	rm -rf .mypy_cache

# auto format the code
format:
	$(BLACK) backend/main.py
	$(BLACK) backend/tests.py

run:
	@echo "MAKE RUN NOT IMPLEMENTED..."

test:
	python3 backend/tests.py
	python3 backend/main.py & # & to run in background. otherwise pointless.
	sleep 10 # let the server finish starting
	newman run Postman.json
	# Selenium not present because it isn't playing nice with Docker right now
	pkill -n -f main.py # kill the server now that we don't need it
	cd my-frontend-app; npm install; npm run test # using cd here is scuffed but it works. install because ci image doesn't have updated packages yet
