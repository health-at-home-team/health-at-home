from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, String, Integer
from flask_cors import CORS
import requests
import time
import urllib
import os
import json
from sqlalchemy import create_engine
from sqlalchemy.sql import text
import flask_restless
from flask_whooshee import *

import init
import models
import scraper
import sys
from search import orderRecipes, filterRecipes
from search import orderWorkouts, filterWorkouts
from search import orderArticles, filterArticles


PAGESIZE = 20  # TODO this and other constants to file: constant.py?

# bring environment variables, model definitions, ... into our namespace
# because those prefixes are annoying
ENV_TYPE = init.ENV_TYPE

KEY_FOOD = init.KEY_FOOD
KEY_NEWS = init.KEY_NEWS
KEY_OURS = init.KEY_OURS

app = init.app
db = init.db
ma = init.ma

Recipe = models.Recipe
recipe_schema = models.recipe_schema
recipes_schema = models.recipes_schema
Workout = models.Workout
workout_schema = models.workout_schema
workouts_schema = models.workouts_schema
Article = models.Article
article_schema = models.article_schema
articles_schema = models.articles_schema
GenericError = models.GenericError

db.create_all()  # This should run fine - checks for table existence first

# Return our frontend pages to the user
# Requires fresh version of site be built for production before use
#       cd my-frontend-app; npm install; npm run build; cd ..
@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    return render_template("index.html")


# Error handler so the user actually finds out why their call didn't work
# Just pass an error to them in a JSON format
@app.errorhandler(GenericError)
def handleGenericError(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


# A controller for scraping data from sources into our database
# all packaged into a single endpoint; arguably bad but it works
#       if anything in here doesn't work use older version to scrape
#       intended functionality is the same as before (Oct 29)
@app.route("/api/testScrape", methods=["POST"])
def testScraper():
    authKey = request.args.get("authKey", "", type=str)
    if authKey != KEY_OURS:
        raise GenericError(
            "Sorry, you do not have authorization for this. "
            + "If you have an auth key, make sure you entered it correctly.",
            status_code=401,
        )  # doesn't fit 401 perfectly but good enough for now
    type = request.args.get("type", "No type provided", type=str)
    if type == "recipe":
        offset = request.args.get("offset", 0, type=int)
        return scraper.storeRecipes(offset)
    elif type == "workout":
        offset = request.args.get("offset", 0, type=int)
        return scraper.storeWorkouts(offset)
    elif type == "article":
        page = request.args.get("page", 1, type=int)
        return scraper.storeArticles(page)
    else:
        raise GenericError(
            "Make sure to correctly specify the model you want to scrape."
        )


@app.route("/api/recipeInfo", methods=["GET"])
def getRecipe():
    id = request.args.get("id", -1, type=int)
    if validateID(id):
        recipe = Recipe.query.filter_by(id=id).first_or_404(
            description=f"Could not find recipe with id {id}"
        )
        response = recipes_schema.dump(recipe, many=False)
        return jsonify(response)
    else:
        raise GenericError("Please specify a valid ID (a positive integer)")


@app.route("/api/recipes", methods=["GET"])
def getRecipes():
    page = request.args.get("page", 1, type=int)

    cuisine = request.args.get("cuisine", type=str)
    dietName = request.args.get("dietName", type=str)
    maxReadyTime = request.args.get("maxReadyTime", type=int)
    minCalories = request.args.get("minCalories", type=int)
    maxCalories = request.args.get("maxCalories", type=int)
    pricePerServing = request.args.get("pricePerServing", type=int)
    orderName = request.args.get("order_by", type=str)
    ordering = request.args.get("ordering", type=str)
    title = request.args.get("title", type=str)

    # maxCalories = sys.maxsize if not maxCalories else maxCalories
    if not minCalories:
        minCalories = 0
    if not maxCalories:
        maxCalories = sys.maxsize

    # filter recipes by specified parameters
    query = Recipe.query
    query = filterRecipes(
        query,
        cuisine,
        dietName,
        maxReadyTime,
        pricePerServing,
        maxCalories,
        minCalories,
    )

    orderBy = False if ordering == "desc" else True

    # search for a recipe title
    if title:
        query = query.whooshee_search(title)
    query = orderRecipes(query, orderName, orderBy)

    recipes = query.paginate(page, PAGESIZE, False)
    response = jsonify(recipes_schema.dump(recipes.items))
    response.headers["total-pages"] = recipes.pages
    return response


# Returns a list of workouts from our database
# Paginated
# Supports filter by category, text search through fields, sort/order by fields
#       Sorted categories must be whitelisted in orderables
#           This is to prevent SQL injection - see implementation of order by
#       Text search by Flask-Whooshee
@app.route("/api/workouts", methods=["GET"])
def getWorkouts():
    page = request.args.get("page", 1, type=int)
    language = request.args.get("language", type=str)
    category = request.args.get("category", type=str)
    muscles = request.args.getlist("muscles", type=str)
    for muscle in muscles:
        if not len(muscle):
            muscles.remove(muscle)
    equipment = request.args.getlist("equipment", type=str)
    for equip in equipment:
        if not len(equip):
            equipment.remove(equip)

    # all languages, categories if no filtering specified
    # contains() to require all params in a list, from an array (A1 & A2 & ...)
    query = Workout.query
    # filter workouts
    query = filterWorkouts(query, language, category, muscles, equipment)

    order_by = request.args.get("order_by", type=str)
    ordering = request.args.get("ordering", type=str)
    name = request.args.get("name", type=str)
    if name:
        relevance_amount = 10
        if order_by:
            relevance_amount = 0
        query = query.whooshee_search(name, order_by_relevance=relevance_amount)

    # order workouts
    query = orderWorkouts(query, ordering, order_by)

    workouts = query.paginate(page, PAGESIZE, False)
    response = jsonify(workouts_schema.dump(workouts.items))
    response.headers["total-pages"] = workouts.pages
    return response


# Returns a single workout to the user, if it exists
@app.route("/api/workoutInfo", methods=["GET"])
def getWorkout():
    id = request.args.get("id", -1, type=int)
    if validateID(id):
        workout = Workout.query.filter_by(id=id).first_or_404(
            description=f"Could not find workout with id {id}"
        )
        response = workouts_schema.dump(workout, many=False)
        return jsonify(response)
    else:
        raise GenericError("Please specify a valid ID (a positive integer)")


@app.route("/api/healthnews", methods=["GET"])
def getArticles():
    page = request.args.get("page", 1, type=int)

    # Filtering
    source = request.args.get("source", type=str)
    publishedAt = request.args.get("publishedAt", type=str)
    query = Article.query
    query = filterArticles(query, source, publishedAt)

    # Searching - fuzzy search
    title = request.args.get("title", type=str)
    if title:
        query = query.whooshee_search(title)

    # Sorting - use order by
    order_by = request.args.get("order_by", "id", type=str)
    ordering = request.args.get("ordering", "asc", type=str)
    query = orderArticles(query, ordering, order_by)

    articles = query.paginate(page, PAGESIZE, False)
    response = jsonify(articles_schema.dump(articles.items))
    response.headers["total-pages"] = articles.pages
    return response


@app.route("/api/healthnewsInfo", methods=["GET"])
def getArticle():
    id = request.args.get("id", -1, type=int)
    if validateID(id):
        article = Article.query.filter_by(id=id).first_or_404(
            description=f"Could not find article with id {id}"
        )
        response = article_schema.dump(article, many=False)
        return jsonify(response)
        # return article_schema.jsonify(article) # ???
    else:
        raise GenericError("Please specify a valid ID (a positive integer)")


# Make sure the ID the user wants to look for is vaguely reasonable
# Trivial, but maybe one day we will have a stupid and convoluted ID system
def validateID(id):
    return id > 0


# Run Server
if __name__ == "__main__":
    debug = False if ENV_TYPE == "production" else True
    app.run(host="0.0.0.0", port=80, threaded=True, debug=debug)
