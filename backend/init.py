from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, String, Integer
from flask_cors import CORS
import requests
import time
import urllib
import os
import json
from sqlalchemy import create_engine
import flask_restless
from flask_whooshee import Whooshee

# OK to define these in other files
# Not super OK, but not the end of the world for this to be public
ENV_TYPE = os.getenv("ENV_TYPE")
DB_URL = (
    "healthathomedb.coacny3dggy8.us-east-1.rds.amazonaws.com"
    # os.getenv('DB_ADDRESS')
)
DB_PORT = "5432"  # os.getenv('DB_PORT')
DB_NAME = "postgres"  # os.getenv('DB_NAME')

# Secret, real values as environment variables only
USERNAME = os.getenv("DB_USERNAME")
PASSWORD = os.getenv("DB_PASSWORD")
KEY_FOOD = os.getenv("API_KEY_FOOD")
KEY_NEWS = os.getenv("API_KEY_NEWS")
KEY_OURS = os.getenv("API_KEY_OURS")

basedir = os.path.abspath(os.path.dirname(__file__))
app = Flask(
    __name__,
    static_folder="../my-frontend-app/build/static",
    template_folder="../my-frontend-app/build",
)
whooshee = Whooshee(app)  # used for text search
CORS(app)

app.config[
    "SQLALCHEMY_DATABASE_URI"
] = f"postgresql+psycopg2://{USERNAME}:{PASSWORD}@{DB_URL}:{DB_PORT}/{DB_NAME}"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)

manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)
