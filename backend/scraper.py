from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, String, Integer
from flask_cors import CORS
import requests
import time
import urllib
import os
import json
from sqlalchemy import create_engine
import flask_restless

import init
import models
import search

# bring environment variables, model definitions, ... into our namespace
# because those prefixes are annoying
db = init.db
KEY_FOOD = init.KEY_FOOD
KEY_NEWS = init.KEY_NEWS

Recipe = models.Recipe
RecipeSchema = models.RecipeSchema
Workout = models.Workout
# WorkoutSchema = models.WorkoutSchema
Article = models.Article
# ArticleSchema = models.ArticleSchema

removeHTMLTags = search.removeHTMLTags
convertLabel = search.convertLabel
convertLabels = search.convertLabels
workoutMuscles = search.workoutMuscles
workoutLicenses = search.workoutLicenses
workoutLanguages = search.workoutLanguages
workoutCategories = search.workoutCategories
workoutEquipments = search.workoutEquipments

# Add the items we got from our data sources to our own database tables
def storeRecipes(offset):
    response = scrapeRecipes(offset)
    # Response is constructed as a list of dictionaries of desired properties
    # Different from how Workouts and Articles are constructed
    for r in response:
        recipe = Recipe(**r)
        db.session.add(recipe)
    db.session.commit()
    return f"Completed offset range {offset + 1} to {offset + 100}"


# Add the items we got from our data sources to our own database tables
def storeWorkouts(offset):
    response = scrapeWorkouts(offset)
    for r in response["results"]:
        workout = Workout(**r)
        db.session.add(workout)
    db.session.commit()
    return f"""Finished range {offset + 1} to {offset + 999}.
            Next offset: {offset + 999}"""


# Add the items we got from our data sources to our own database tables
def storeArticles(page):
    response = scrapeArticles(page)
    for r in response["articles"]:
        r["source"] = r["source"]["name"]
        article = Article(**r)
        db.session.add(article)
    db.session.commit()
    return f"Completed page {page}"


# Gather and prepare items for storage in our own database
def scrapeRecipes(offset):
    recipe_params = {
        "addRecipeNutrition": "true",
        "offset": offset,
        "addRecipeInformation": "true",
        "apiKey": KEY_FOOD,
        "number": "100",
    }
    raw = requests.get(
        "https://api.spoonacular.com/recipes/complexSearch", params=recipe_params
    ).json()
    # Build a new list of copied information
    # Do this because Spoonacular's returns are super bloated
    response = []
    # Take information we want instead of remove fields we don't want
    for r in raw["results"]:
        recipe = {}
        for field in RecipeSchema.Meta.fields:
            recipe[field] = r[field]
        recipe["summary"] = removeHTMLTags(r["summary"])
        response.append(recipe)
    return response


# Gather and prepare items for storage in our own database
def scrapeWorkouts(offset):  # how to handle source error/rate limit?
    workoutparams = {"status": "2", "offset": offset, "ordering": "id", "limit": "999"}
    response = requests.get(
        "https://wger.de/api/v2/exercise", params=workoutparams
    ).json()
    for r in response["results"]:
        # Process responses into a more search-friendly format
        r["category"] = convertLabel(r["category"], workoutCategories)
        r["equipment"] = convertLabels(r["equipment"], workoutEquipments)
        r["muscles"] = convertLabels(r["muscles"], workoutMuscles)
        r["muscles_secondary"] = convertLabels(r["muscles_secondary"], workoutMuscles)
        r["language"] = convertLabel(r["language"], workoutLanguages)
        r["license"] = convertLabel(r["license"], workoutLicenses)
        r["description"] = removeHTMLTags(r["description"])
        # Add an image link if available
        imgparams = {"exercise": r["id"], "status": "2"}
        images = requests.get(
            "https://wger.de/api/v2/exerciseimage", params=imgparams
        ).json()
        if images["count"]:
            r["exercise_images"] = images["results"][0]["image"]
        else:
            r["exercise_images"] = ""
        # Drop stuff we and our users don't care about
        del r["status"]
        del r["name_original"]
        del r["creation_date"]
        del r["uuid"]
        # Quality of life printout when running locally
        print("Prepared workout with id " + str(r["id"]))
    return response


# Gather and prepare items for storage in our own database
def scrapeArticles(page):
    articleparams = {"qInTitle": "nutrition", "page": page, "apiKey": KEY_NEWS}
    # This one doesn't change anything. Considered fine to store as-is
    response = requests.get(
        "http://newsapi.org/v2/everything", params=articleparams
    ).json()
    return response
