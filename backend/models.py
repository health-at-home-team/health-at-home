from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, String, Integer
from sqlalchemy.dialects.postgresql import ARRAY
from flask_cors import CORS
import requests
import time
import urllib
import os
import json
from sqlalchemy import create_engine
import flask_restless
from flask_whooshee import Whooshee

import init

# bring environment variables, model definitions, ... into our namespace
# because those prefixes are annoying
db = init.db
ma = init.ma
whooshee = init.whooshee

# Recipe Model
@whooshee.register_model("title", "summary")
class Recipe(db.Model):
    __tablename__ = "recipes"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    cuisines = db.Column(ARRAY(db.String()))
    diets = db.Column(ARRAY(db.String()))
    pricePerServing = db.Column(db.Integer)
    readyInMinutes = db.Column(db.Integer)
    servings = db.Column(db.Integer)
    image = db.Column(db.String())
    summary = db.Column(db.String())
    nutrition = db.Column(db.JSON())
    analyzedInstructions = db.Column(db.JSON())
    imageType = db.Column(db.String())
    sourceUrl = db.Column(db.String())
    aggregateLikes = db.Column(db.Integer)


class RecipeSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "title",
            "cuisines",
            "diets",
            "pricePerServing",
            "readyInMinutes",
            "servings",
            "image",
            "summary",
            "nutrition",
            "analyzedInstructions",
            "imageType",
            "sourceUrl",
            "aggregateLikes",
        )


recipe_schema = RecipeSchema()
recipes_schema = RecipeSchema(many=True)

# Register our model with Flask-Whooshee so it can index the fields we want
# Complains when trying to index the commented fields
@whooshee.register_model(
    "name",
    "category",
    "description",
    "language"
    # , "license", "license_author"
)
class Workout(db.Model):
    __tablename__ = "workouts"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)
    category = db.Column(db.String())
    equipment = db.Column(ARRAY(db.String()))
    muscles = db.Column(ARRAY(db.String()))
    muscles_secondary = db.Column(ARRAY(db.String()))
    description = db.Column(db.String())
    exercise_images = db.Column(db.String())
    language = db.Column(db.String())
    license = db.Column(db.String())
    license_author = db.Column(db.String())


class WorkoutSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "name",
            "category",
            "equipment",
            "muscles",
            "muscles_secondary",
            "description",
            "exercise_images",
            "language",
            "license",
            "license_author",
        )


workout_schema = WorkoutSchema()
workouts_schema = WorkoutSchema(many=True)

# Article Model
@whooshee.register_model("title", "author", "description")
class Article(db.Model):
    __tablename__ = "articles"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String())
    author = db.Column(db.String())
    source = db.Column(db.String())
    description = db.Column(db.String())
    publishedAt = db.Column(db.String())
    content = db.Column(db.String())
    country = db.Column(db.String())
    url = db.Column(db.String())
    urlToImage = db.Column(db.String())


class ArticleSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "title",
            "author",
            "source",
            "description",
            "publishedAt",
            "content",
            "url",
            "urlToImage",
        )


article_schema = ArticleSchema()
articles_schema = ArticleSchema(many=True)

db.create_all()

print("!!REINDEXING WHOOSHEE STUFF!! THIS IS GENERATING 2MB OF STUFF...")
whooshee.reindex()
print("OK WE ARE DONE REINDEXING HAVE A NICE DAY :)")

# A general purpose error to retuurn when we want to say something is wrong
class GenericError(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    # required to be able to return this, since we need something jsonifiable
    def to_dict(self):
        if self.payload is None:
            self.payload = ()
        errDict = dict(self.payload)
        errDict["message"] = self.message
        return errDict
