import React, {Component} from 'react'
import {Col, Container, Row, Image } from 'react-bootstrap'
import {RouteComponentProps } from 'react-router-dom'
import axios from 'axios';


export class WorkoutInstance extends Component {
    state = {
        info: {
            name: "",
            description: "",
            category: "",
            primary_muscle: "",
            secondary_muscle: "",
            equipment: "",
            image: "",
            license: "",
            license_author:"",
        },
        loading: false,
    };
    
    id;
    constructor(props: RouteComponentProps<{id}> ) {
        super(props);
        const {id} = props.match.params;
        this.id = id;
    }
    componentDidMount() {
        const getPosts = async () => {
            {this.setState({loading: true})};
            const results = await axios.
                get("https://api.healthathome.me/api/"
                .concat("workoutInfo?id=").concat((this.id)));
            const tempData = results.data;
            //check if the API has an image
            if (tempData.exercise_images === undefined || 
                tempData.exercise_images.length == 0) {
                //if there's isn't one available, add the generic one
                var workout_info = {
                    name: tempData.name,
                    description: tempData.description,
                    category: tempData.category,
                    primary_muscle: tempData.muscles,
                    secondary_muscle: tempData.muscles_secondary,
                    equipment: tempData.equipment,
                    image: "https://images.pexels.com/photos/209969"
                        .concat("/pexels-photo-209969.jpeg?")
                        .concat("auto=compress&cs=tinysrgb&dpr=2&w=500"),
                    license: tempData.license,
                    license_author: tempData.license_author,
                }
                this.setState({info: workout_info, loading: false});
            } else {
                //image available, so just use that
                workout_info = {
                    name: tempData.name,
                    description: tempData.description,
                    category: tempData.category,
                    primary_muscle: tempData.muscles,
                    secondary_muscle: tempData.muscles_secondary,
                    equipment: tempData.equipment,
                    image: tempData.exercise_images,
                    license: tempData.license,
                    license_author: tempData.license_author,
                };
                this.setState({info: workout_info, loading: false});
            }
        };
        getPosts();
    }

    render() {
        const {info, loading} = this.state;

        return (

            // Displays the information on the instances pages
            <Container className = "p-1 bg-light border border-dark rounded-lg">
                <h2 className="text-center"> {info.name}</h2>
                <hr></hr>
                <p> <mark>Description:</mark> {info.description}</p>
                <p><mark>Category:</mark> {info.category} </p>
                <p><mark>Equipment:</mark> {info.equipment}</p>
                <p><mark>Muscles Primary:</mark> {info.primary_muscle}</p>
                <p><mark>Muscles Secondary:</mark> {info.secondary_muscle}</p> 
                <hr></hr>
                <Row className="justify-content-center my-3">
                    <Col sm={4}>
                        <Image src={info.image} thumbnail />
                    </Col>
                </Row>
                <hr></hr>
                <p>License: {info.license}</p>
                <p>License Author: {info.license_author}</p>      
            </Container>

        )
    }

}