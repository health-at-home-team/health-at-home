import React, {Component} from 'react'
import { Button, Col, Container, Row, Image} from 'react-bootstrap'
import { RouteComponentProps } from 'react-router-dom'
import axios from 'axios';


export class RecipeInstance extends Component {
    //set state with given attributes
    state = {
        info: {
            title: "",
            cuisines: "",
            diets: "",
            summary: "",
            image: "",
            sourceUrl: ""
        },
        loading: false,
    };
    
    //set up routing
    id;
    constructor(props: RouteComponentProps<{id}> ) {
        super(props);
        const {id} = props.match.params;
        this.id = id;
    }
    //call the API to get instances of recipe model
    componentDidMount() {
        const getPost = async () => {
            this.setState({loading: true});
            const results = await axios
            .get('https://api.healthathome.me/api/recipeInfo?id='
            .concat((this.id)));
            this.setState({info: results.data, loading: true});
        };
        getPost();
    }
    render() {
        const {info} = this.state;
        //set up instance page
        return (
            <Container className="mb-5">
                <h3>{info.title}</h3>
                <small>Cuisines: {info.cuisines}</small>
                <em className="mx-2">-</em>
                <small>Diets: {info.diets}</small> <br/>
                <hr/>
                <Row className="justify-content-center my-3">
                    <Col lg={6}>
                        <Image src={info.image} fluid />
                    </Col>
                </Row>
                {/* links to source website for each recipe */}
                <p>{info.summary}</p>
                <Button variant="Primary" 
                className="bg-primary text-white" href={info.sourceUrl}>
                    View this recipe!</Button>

            </Container>
    )
    }
}
