import React from 'react'
import { Col, Row, Figure, Container, Button, 
    ButtonToolbar } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { SearchBar } from './Components/Home/Search/SearchBar'
import apple from './references/HomePage/apple.jpg'
import eggplant from './references/HomePage/eggplant.jpg'
import foodspread from './references/HomePage/foodspread.jpg'
import friedrice from './references/HomePage/friedrice.jpg'
import fruit from './references/HomePage/fruit.jpg'
import greenjuice from './references/HomePage/greenjuice.jpg'
import house from './references/HomePage/house.jpg'
import news from "./references/HomePage/news.jpg"
import yoga from "./references/HomePage/yoga.jpg"


//home page including a search bar for general sitewide search
export const Home = () => (
    <Container>
        <h1 className="text-center">Welcome to Health at Home!</h1>
        <hr></hr>
        <br></br>
        <SearchBar/>
        <br></br>
        <h5>Live your BEST life with Health at Home by your side.</h5>
        <br></br>
        <Row>
            <Col md="4">
                <Figure>
                    <div className="col text-center">
                    <Link to= '/recipes'> 
                        <ButtonToolbar>
                            <Button variant='light' block>
                                Recipes</Button>
                        </ButtonToolbar>
                    </Link>
                    </div>
                    <Figure.Image src={apple}/>
                </Figure>
            </Col>
            <Col md="4">
                <div className = "col text-center">
                <Link to= '/workouts'> 
                    <ButtonToolbar>
                        <Button variant='light' block>
                            Workouts</Button>
                    </ButtonToolbar>
                </Link>
                </div>
                <img src={yoga} className="img-fluid" />
            </Col>
            <Col md="4">
                <div className = "col text-center">
                <Link to= '/healthnews'> 
                    <ButtonToolbar>
                        <Button variant='light' block>
                            Health News</Button>
                    </ButtonToolbar>
                </Link>
                </div>
                <img src={news} className="img-fluid" />
            </Col>
        </Row>
        <Row>
            <Col md="4">
                <Figure>
                    <Figure.Image src={friedrice}/>
                </Figure>
            </Col>
            <Col md="4">
                <img src={house} className="img-fluid"/>
            </Col>
            <Col md="4">
                <img src={greenjuice} className="img-fluid"/>
            </Col>
        </Row>
        <Row>
            <Col md="4">
                <img src={fruit} className="img-fluid"/>
            </Col>
            <Col md="4">
                <img src={foodspread} className="img-fluid"/>
            </Col>
            <Col md="4">
                <img src={eggplant} className="img-fluid"/>
            </Col>
        </Row>
    </Container>
)
