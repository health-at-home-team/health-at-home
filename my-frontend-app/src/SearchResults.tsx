import React, { Component } from 'react'
import {Accordion, Button, Col, Container, Row} from 'react-bootstrap'
import { ArticleTeaser } from './Components/HealthNews/ArticleTeaser';
import { SearchBar } from './Components/Home/Search/SearchBar';
import { Posts } from './Components/PostsWorkouts';
import { PostsRecipes} from './Components/Posts_Recipes';


export class SearchResults extends Component {

    searchParams : URLSearchParams;

    constructor(props) {
        super(props);
        this.state={}

        var queryString = props.location.search;

        this.searchParams = new URLSearchParams(queryString);
    }

    componentDidMount = () => {
        this.setState({});
    }

    render() {
        const healthNewsSearchParams = {"title": this.searchParams.
            get("search")?.toString()};
        const RecipesSearchParams = {"name": this.searchParams
            .get("search")?.toString()};
            //returns general search results page 
        const WorkoutsSearchParams = {"name": this.searchParams.
            get("search")?.toString()};
        return (
            <Container>
                <h1>Welcome to the search results page</h1>
                <SearchBar/>
                <hr/>
                <Accordion>
                    <Row>
                        {/* Recipes Option */}
                        <Col sm={4} xs={12} className="py-2">
                            <Accordion.Toggle as={Button} eventKey="1">
                                Click to see Recipes results
                            </Accordion.Toggle>
                        </Col>
                        {/* Workouts Option */}
                        <Col sm={4} xs={12} className="py-2">
                            <Accordion.Toggle as={Button} eventKey="2">
                                Click to see Workouts results
                            </Accordion.Toggle>
                        </Col>
                        {/* Health News Option */}
                        <Col sm={4} xs={12} className="py-2">
                            <Accordion.Toggle as={Button} eventKey="3">
                                Click to Health News results
                            </Accordion.Toggle>
                        </Col>
                    </Row>
                    <Row>
                        {/* returns recipes search results page  */}
                        <Accordion.Collapse eventKey="1">
                            <PostsRecipes 
                                {...{queryParams: RecipesSearchParams}}/>
                        </Accordion.Collapse>
                        {/* returns workouts search results page */}
                        <Accordion.Collapse eventKey="2">
                            <Posts {...{queryParams: WorkoutsSearchParams}}/>
                        </Accordion.Collapse>
                        {/* returns health news search results page */}
                        <Accordion.Collapse eventKey="3">
                            <ArticleTeaser 
                                {...{queryParams: healthNewsSearchParams}}/>
                        </Accordion.Collapse>
                    </Row>
                </Accordion>
            </Container>
        )
    }
}