import React, {Component} from 'react'
import {Table, Row, Spinner} from 'react-bootstrap'
import {RecipesSort, RecipesFilterDiets, 
    RecipesFilterCuisines} from './Recipes/RecipesSearch'
import axios from 'axios';
import {HighlighterText} from './Highlight'
import Pagination from "react-pagination-bootstrap";


//list containing all possible diet filter options
const diets = ["paleolithic", "primal", "gluten free", "pescatarian", "vegan"]
//list containing all possible cuisine filter options
const cuisines = ["Mexican", "Chinese", "Asian", "American", "African", 
"Creole", "Cajun", "Indian", "Mediterranean", "Italian", "European", "Irish", 
"Vietnamese", "Thai", "Korean", "Greek", "Spanish", "French"]

const dietFilters = ["paleolithic", "primal", "gluten free", "pescatarian", "vegan"];

export class PostsRecipes extends Component {
    state = {
        //attributes of each row on recipes model page
        posts: [{
            id: "",
            title: "",
            pricePerServing: "",
            diets: [],
            cuisines: [],
            readyInMinutes: "",
            summary: "",
            image: "",
            sourceUrl: "",
            aggregateLikes: ""
        }],
        loading: true,
        currentPage: 1
    };

    //declare all variables for user input
    currentPage: number = 1;
    queryParams: {} = {};
    sortParams: string;
    sortingOrder: string;
    filterParamsDiets: string;
    filterValueParamsDiets: string;
    filterParamsCuisines: string;
    filterValueParamsCuisines: string;
    stringQuery: string;



    //initialize variables for user input
    constructor(props) {
        super(props);
        this.sortParams = "";
        this.filterParamsDiets = "";
        this.filterValueParamsDiets = "";
        this.filterParamsCuisines = "";
        this.filterValueParamsCuisines = "";
        this.stringQuery = "";
        this.sortingOrder = "";
    }

    //used for search
    componentWillReceiveProps = (props) => {
        this.queryParams = props.queryParams;
        this.getPosts();
    }

    //method to generate query strings for API call
    generateQueryStrings = () => {
        var result: string[] = [];
        for (var key in this.queryParams) {
            result.push(key.concat("=").concat(this.queryParams[key]));
        }
        return result.join("&");
    }

    createStringQuery = () => {
        var result: string = "";
        for (var key in this.queryParams) {
            result += (this.queryParams[key]);
        } 
        return result;
    }

    handlePageChange = (pageNumber) => {
        this.currentPage = pageNumber;
        this.getPosts();
      }
    

    //main method where API is called
    getPosts = async () => {
        this.setState({loading: true});
        //strings for searching, filtering, sorting concatenated 
        //to form one get call
        const pageString = '&page='.concat(this.currentPage.toString());
        const queryStrings = this.generateQueryStrings();
        this.stringQuery = this.createStringQuery();
        const filterString = this.filterParamsDiets.concat("=")
        .concat(this.filterValueParamsDiets)
        .concat("&").concat(this.filterParamsCuisines)
        .concat("=").concat(this.filterValueParamsCuisines);
        const url = 'https://api.healthathome.me/api/recipes?'
        .concat(queryStrings).concat("&order_by=")
        .concat(this.sortParams).concat("&ordering=").concat(this.sortingOrder)
        .concat("&").concat(filterString).concat(pageString);
        const results = await axios.get(url);
        this.setState({posts: results.data, loading: false});
    };

    componentDidMount() {
        this.getPosts();
    }

    //handles sort capabilities for recipes
    handleSort = (e) => {
        this.sortParams = e.target.value.toString();
        if(this.sortParams === "readyInMinutesAsc"){
            this.sortParams = "readyInMinutes"
            this.sortingOrder = "asc"
        } else if (this.sortParams === "readyInMinutesDesc"){
            this.sortParams = "readyInMinutes"
            this.sortingOrder = "desc"
        } else if (this.sortParams === "pricePerServingAsc"){
            this.sortParams = "pricePerServing"
            this.sortingOrder = "asc"
        } else if (this.sortParams === "pricePerServingDesc"){
            this.sortParams = "pricePerServing"
            this.sortingOrder = "desc"
        } else if (this.sortParams === "popularityAsc"){
            this.sortParams = "popularity"
            this.sortingOrder = "asc"
        } else if (this.sortParams === "popularityDesc"){
            this.sortParams = "popularity"
            this.sortingOrder = "desc"
        }
        this.getPosts();
    }

    //handles filter capabilities for recipes (diets)
    handleFilterDiets = (e) => {
        this.filterValueParamsDiets = e.target.value.toString();
        //diets
        for(var i = 0; i < diets.length; i++){
            if(this.filterValueParamsDiets === diets[i]){
                this.filterParamsDiets = "dietName";
            }
        }
        this.getPosts();
    }

    //handles filter capabilities for recipes (cuisines)
    handleFilterCuisines = (e) => { 
        this.filterValueParamsCuisines = e.target.value.toString();
        //cuisines
        for(var i = 0; i < cuisines.length; i++){
            if(this.filterValueParamsCuisines === cuisines[i]){
                this.filterParamsCuisines = "cuisine";
            }
        }
        this.getPosts();
    }


    render() {

        //display loading on website when page is loading
        if (this.state.loading) {
            return (
                <Row className="justify-content-center">
                    <Spinner animation="border" role="status"/>
                    <h4>Loading...</h4>
                </Row>
            )
        }


        return (
        //creates a dynamically responsive table capable of filtering,
        //sorting, and searching
            <Row className="justify-content-center">
            <RecipesSort handleSort = {this.handleSort}/>

            <RecipesFilterDiets handleFilterDiets = {this.handleFilterDiets}/>
            <RecipesFilterCuisines handleFilterCuisines = 
            {this.handleFilterCuisines}/>
        

            <Table striped bordered hover>
                <thead>
                    {/* Categories displayed in main recipes table */}
                    <tr>
                    <th>Recipe</th>
                    <th>Price Per Serving(Cents)</th>
                    <th>Diets</th>
                    <th>Cuisines</th>
                    <th>Prep Time (min)</th>
                    <th>Aggregate Likes</th>
                    </tr>
                </thead>
                <tbody>
                    {/* Dynamically generates each row in recipes table */}
                    {this.state.posts.map(post => (
                        <tr key = {post.id}>
                            <td><a href= {String("/recipes/")
                            .concat(post.id)}> <HighlighterText 
                            text = {post.title} 
                            searchValue={this.stringQuery}/> </a></td>
                            <td>{post.pricePerServing}</td>
                            <td>{post.diets.toString()}</td>
                            <td>{post.cuisines.toString()}</td>
                            <td>{post.readyInMinutes}</td>
                            <td>{post.aggregateLikes}</td>
                        </tr>
                    ))}
                </tbody>
            </Table>
             <Pagination
                prevPageText='prev '
                nextPageText='next '
                firstPageText='first '
                lastPageText='last'
                activePage={this.currentPage}
                itemsCountPerPage={10}
                totalItemsCount={260}
                pageRangeDisplayed={10}
                onChange={this.handlePageChange}
            />
         </Row>
            
        )
    }
}