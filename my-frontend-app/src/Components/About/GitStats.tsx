import React from "react"
import {Component} from "react"
import { Card, Col, Container, Image, Row } from "react-bootstrap"

import andrewImage from "../../references/About/andrew.jpg"
import anweshaImage from "../../references/About/anwesha.jpg"
import ghaffarImage from "../../references/About/ghaffar.jpg"
import jonathanImage from "../../references/About/jonathan.png"
import priyaImage from "../../references/About/priya.jpg"
import shrutiImage from "../../references/About/shruti.jpg"

export type MemberInfo = {
    name: string,
    eid: string,
    bio: string,
    gitlab: string,
    roles: string[],
    commits: number,
    issues: number,
    tests: number,
    img: string,
    linkedIn: string,
}

const issuesURL = "https://gitlab.com/api/v4/projects"
.concat("/health-at-home-team%2Fhealth-at-home/issues?per_page=100");
const commitsURL = "https://gitlab.com/api/v4/projects/health-at-home-team%"
.concat("2Fhealth-at-home/repository/commits?ref_name=master&per_page=100");

const membersToIndex : {[key: string]: number} = {
    "Jonathan Serbent": 0,
    "Shruti V": 1,
    "Shruti Vellaturi": 1,
    "Priya K Patel": 2,
    "Anwesha Roy": 3,
    "AnweshaRoy": 3,
    "Ghaffar Balogun": 4,
    "Andrew Chen": 5,
    "heisenbee13": 5,
}

export class GitStats extends Component{
    isLoaded: boolean;
    members: MemberInfo[];
    totalIssues: number;
    totalCommits: number;
    totalTests: number;


    constructor(props: string) {
        super(props);

        this.isLoaded = false;
        this.members = [
            {
                name: "Jonathan Serbent",
                eid: "jas242456",
                bio: "Class of 2022, heavily interested in full-stack web"
                .concat(" development. Really passionate about music and")
                .concat("wakeboarding."),
                gitlab: "jonathanserbent",
                roles: ["front-end", "note-taker"],
                commits: 0,
                issues: 0,
                tests: 0,
                img: jonathanImage,
                linkedIn: "https://www.linkedin.com/in/jonathan-serbent"
                .concat("-66134b174/"),
            },
            {
                name: "Shruti Vellaturi",
                eid: "",
                bio: "I'm Shruti! I am a junior in CS and in my free time"
                .concat(" I love to travel, read, or binge Netflix."),
                gitlab: "",
                roles: ["front-end"],
                commits: 0,
                issues: 0,
                tests: 0,
                img: shrutiImage,
                linkedIn: "https://www.linkedin.com/in/shruti-vellaturi",
            },
            {
                name: "Priya Patel",
                eid: "",
                bio: "I’m a senior majoring in computer science and "
                .concat("minoring in coffee consumption. I’m interested")
                .concat(" in full-stack development. When I’m not staring")
                .concat(" at my computer screen of code or k-dramas, I enjoy")
                .concat(" spending time with friends and appreciating nature."),
                gitlab: "",
                roles: ["back-end", "scrum leader"],
                commits: 0,
                issues: 0,
                tests: 0,
                img: priyaImage,
                linkedIn: "https://www.linkedin.com/in/priyapatelcs/",
            },
            {
                name: "Anwesha Roy",
                eid: "",
                bio: "Hi I'm Anwesha, a junior in CS, and I like spending time"
                .concat(" outdoors, reading, and trying new hobbies!"),
                gitlab: "",
                roles: ["front-end"],
                commits: 0,
                issues: 0,
                tests: 0,
                img: anweshaImage,
                linkedIn: "https://www.linkedin.com/in/anwesharoy/",
            },
            {
                name: "Abdul-Ghaffar Balogun",
                eid: "gb23427",
                bio: "My name is Abdul-Ghaffar Balogun and I am a Senior"
                .concat(" Computer Science major at the University of Texas")
                .concat(" at Austin. Some of my hobbies include sight-seeing")
                .concat(" and playing soccer."),
                gitlab: "ghaffarbalogun",
                roles: ["back-end"],
                commits: 0,
                issues: 0,
                tests: 0,
                img: ghaffarImage,
                linkedIn:"https://www.linkedin.com/in/ghaffar-balogun-"
                .concat("6104a2171/"),
            },
            {
                name: "Andrew Chen",
                eid: "",
                bio: "Yet another CS major. Graduating in 2022. Interested"
                .concat(" in game design and machine learning. Enjoys video")
                .concat(" games, thinking about unaffordable computer ")
                .concat("hardware, and being asked about what he would")
                .concat(" be doing a Sunday morning."),
                gitlab: "",
                roles: ["backend", "api-design"],
                commits: 0,
                issues: 0,
                tests: 0,
                img: andrewImage,
                linkedIn: "https://www.linkedin.com/in/andrew-chen-208606192",
            },
        ];

        this.totalIssues=0;
        this.totalCommits=0;
        this.totalTests=5 + 8 + 20; // Frontend + backend + postman
    }

    componentDidMount() {
        var self = this;
        console.log("HELLO");
        fetch(commitsURL)
            .then((response) => response.json())
            .then((data) => {
                self.totalCommits = data.length;
                for (var commit of data) {
                    var name :string = commit.committer_name;
                    var memberIndex = membersToIndex[name];

                    if (memberIndex == null) {
                        console.log("Name not found: " + name);
                        continue
                    }
                    self.members[memberIndex].commits += 1;
                }
                self.setState({}); // Will reload the page
            });

        fetch(issuesURL)
            .then((response) => response.json())
            .then((data) => {
                self.totalIssues = data.length;

                for (var issue of data) {
                    for (var assignee of issue.assignees)
                    {
                        var name : string = assignee.name;
                        var memberIndex = membersToIndex[name];

                        if (memberIndex == null) {
                            console.log("Name not found: " + name);
                            continue;
                        }
                        self.members[memberIndex].issues += 1;
                    }
                }

                self.setState({})
            });
    }

    render() {
        return (
            <Container fluid>
                <Row className = "my-3 justify-content-center">
                    <Col md={4} className = "p-3 text-center text-dark">
                        <h2>Commits</h2>
                        <h3>{this.totalCommits}</h3>
                    </Col>
                    <Col md={4} className = "p-3 text-center text-dark">
                        <h2>Issues</h2>
                        <h3>{this.totalIssues}</h3>
                    </Col>
                    <Col md={4} className = "p-3 text-center text-dark">
                        <h2>Tests</h2>
                        <h3>{this.totalTests}</h3>
                    </Col>
                </Row>
                <hr/>

                <Row className="my-3">
                    <Col xs={12} className = "text-center">
                        <h2>Our Team</h2>
                        <h5>Phase 4 Leader: Anwesha Roy</h5>

                    </Col>
                    {this.members.map(member => (
                        <Col lg={4} md={6} className="p-2"  key={member.name}>
                            <Container className 
                            = "p-1 bg-light border border-dark rounded-lg">
                                <h4><Card.Link href={member.linkedIn}>
                                    {member.name}</Card.Link></h4>
                                {/* <h4>{member.name}</h4> */}
                                <Image 
                                className = "rounded-lg border border-dark"
                                    src={member.img} fluid></Image>
                                <p>{member.bio}</p>
                                <p>Member roles:</p>
                                <ul>
                                    {member.roles.map((role) =>(
                                        <li key={role}>{role}</li>
                                    ))}
                                </ul>
                                <p>Commits: {member.commits}</p>
                                <p>Issues: {member.issues}</p>
                            </Container>
                        </Col>
                    ))}
                </Row>
            </Container>
        );
    }
}