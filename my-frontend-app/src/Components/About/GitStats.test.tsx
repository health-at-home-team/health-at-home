import React from 'react';

test('Makes API Req', () => {
    return fetch('https://gitlab.com/api/v4/projects/health-at-home-team%2Fhealth-at-home/repository/commits')
    .then((response) => response.json())
    .then((data) => {
        expect(data).toBeDefined();
    });
})