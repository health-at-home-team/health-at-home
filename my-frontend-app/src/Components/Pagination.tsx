import React, {Component} from 'react'
import { Col } from 'react-bootstrap'

export const Pagination = ({ currentPage, nextPage, prevPage}) => {


    return (
        <Col xs={12} className="justify-content-center">
            <nav>
                    {/* links to previous page */}
                <ul className="pagination justify-countent-center">
                    <li className="page-item">
                        <a className="page-link" onClick={() =>
                            prevPage()}>Previous</a>
                    </li>
                    {/* links to current page */}
                    <li className="page-item">
                        <a className="page-link" href="#" >{currentPage}</a>
                    </li>
                    {/* links to next page */}
                    <li className="page-item">
                        <a className="page-link" onClick={() =>
                            nextPage()}>Next</a>
                    </li>
                </ul>
            </nav>
        </Col>
    )
}
