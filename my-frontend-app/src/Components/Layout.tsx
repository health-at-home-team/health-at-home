import React from 'react';
import Container from "react-bootstrap/Container"

/**
 * This allows all the words/paragraphs 
 * in all the pages to be confined to a container. 
 * Same layout throughout website.
 */

export const Layout = (props: { children: React.ReactNode; }) => (
    <Container>
        {props.children}
    </Container>
)