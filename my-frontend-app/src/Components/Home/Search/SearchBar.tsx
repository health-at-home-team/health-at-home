import React, { FormEvent } from 'react'
import {Button, Form, InputGroup} from 'react-bootstrap'

export const SearchBar = () => {

    var searchTerm;
    var searchKey;
    
    const textChangedFunc = (event) => {
        searchKey = event.target.name;
        searchTerm = event.target.value;
    }

    //takes care of events once search button is pressed
    //redirects URL to a new page
    const submitFunc = (event : FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        //build url based on user input once 
        //search button pressed
        var redirectUrlBuilder : string[] = [];
        redirectUrlBuilder.push("/search?");
        redirectUrlBuilder.push(
            searchKey,
            "=",
            searchTerm
        )

        var redirectUrl = redirectUrlBuilder.join("");
        window.location.href = redirectUrl;

    };

    return (
        //search bar implementation
        <Form onSubmit={submitFunc}>
            <InputGroup>
                <Form.Control onChange={textChangedFunc} name="search" 
                type="text" placeholder="Find hundreds of delicious 
                recipes, effective workouts, and the latest health 
                articles!" />
                <InputGroup.Append>
                    <Button type="submit">Search</Button>
                </InputGroup.Append>
            </InputGroup>
        </Form>
    )
}
