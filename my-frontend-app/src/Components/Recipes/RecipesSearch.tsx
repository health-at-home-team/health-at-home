import React from 'react'
import {Button, Form, InputGroup} from 'react-bootstrap'

//search bar on recipes page that accepts user input
export const RecipesSearch = (Props) => {
    return (
        <div>
        <Form onSubmit={Props.submitFunc}>
            <InputGroup>
                <Form.Control onChange={Props.searchFunc} 
                name="title" type="text" 
                placeholder="Search for a recipe here" />
                <InputGroup.Append>
                    <Button type="submit">Search</Button>
                </InputGroup.Append>
            </InputGroup>
        </Form>
        <br></br>
        </div>
    )
}

//sort dropdown on recipes page that allows 
//user interaction
export const RecipesSort = (props) => {

    return (
        <div>
        <Form>
            <Form.Label>Sort By</Form.Label>
            <Form.Control onChange={props.handleSort}
              type="submit" as = "select" >
            <option defaultValue = "">Choose category</option>
            <option value = "readyInMinutesAsc">Prep Time (min) in 
                Ascending Order</option>
            <option value = "readyInMinutesDesc">Prep Time (min) in 
                Descending Order</option>
            <option value = "pricePerServingAsc">Price Per Serving 
                (cents) in Ascending Order</option>
            <option value = "pricePerServingDesc">Price Per Serving 
                (cents) in Descending Order</option>
            <option value = "popularityAsc">Popularity in 
                Ascending Order</option>
            <option value = "popularityDesc">Popularity in 
                Descending Order</option>
            </Form.Control>
        </Form>
        </div>
    )
}

//diet filter dropdown on recipes page that allows 
//user interaction
export const RecipesFilterDiets = (props) => {

    return (
        <div>
        <Form>
            <Form.Label>Diet</Form.Label>
            <Form.Control onChange={props.handleFilterDiets} 
             type="submit" as = "select" >
            <option defaultValue = "">Diets</option>
            <option value = "paleolithic">Paleo</option>
            <option value = "primal">Primal</option>
            <option value = "gluten free">Gluten Free</option>
            <option value = "pescatarian">Pescetarian</option>
            <option value = "vegan">Vegan</option>
            </Form.Control>
        </Form>
</div>
    )
}

//cuisine filter dropdown on recipes page that allows 
//user interaction
export const RecipesFilterCuisines = (props) => {
        return(
            <div>
            <Form>
            <Form.Label>Cuisines</Form.Label>
            <Form.Control onChange={props.handleFilterCuisines} 
             type="submit" as = "select" >
            <option defaultValue = "">Cuisines</option>
            <option value = "Mexican">Mexican</option>
            <option value = "Chinese">Chinese</option>
            <option value = "Asian">Asian</option>
            <option value = "American">American</option>
            <option value = "African">African</option>
            <option value = "Creole">Creole</option>
            <option value = "Cajun">Cajun</option>
            <option value = "Indian">Indian</option>
            <option value = "Mediterranean">Mediterranean</option>
            <option value = "Italian">Italian</option>
            <option value = "European">European</option>
            <option value = "Irish">Irish</option>
            <option value = "Vietnamese">Vietnamese</option>
            <option value = "Thai">Thai</option>
            <option value = "Korean">Korean</option>
            <option value = "Greek">Greek</option>
            <option value = "Spanish">Spanish</option>
            <option value = "French">French</option>
            </Form.Control>
        </Form></div>
        )

    }
       
