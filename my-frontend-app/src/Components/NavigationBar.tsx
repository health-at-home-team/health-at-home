import React from 'react';
import {Nav, Navbar } from 'react-bootstrap';
import styled from 'styled-components';

const Styles = styled.div`
    .navbar {
        background-color: #222;
    }
 
    .navbar-brand, .navbar-nav .nav-link {
        color: #bbb;

        &:hover {
            color: white;
        }
    }

`;

export const NavigationBar = () => (
    <Styles>
        <Navbar expand="lg" bg="dark" variant="dark">
            <Navbar.Brand href="/">Health At Home</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    <Nav.Item><Nav.Link href="/">
                        Home</Nav.Link></Nav.Item>
                    <Nav.Item><Nav.Link href="/recipes">
                        Recipes</Nav.Link></Nav.Item>  
                    <Nav.Item><Nav.Link href="/workouts">
                        Workouts</Nav.Link></Nav.Item>
                    <Nav.Item><Nav.Link href="/healthnews">
                        Health News</Nav.Link></Nav.Item>  
                    <Nav.Item><Nav.Link href="/about">
                        About</Nav.Link></Nav.Item>
                    <Nav.Item><Nav.Link href="/visualizations">
                        Our Visualizations</Nav.Link></Nav.Item> 
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    </Styles>
)
