import React, { Component } from 'react'
import { Button, Col, Container, Row, Spinner } from 'react-bootstrap'
import axios from 'axios';
import { WorkoutsFilter, WorkoutsFilterEquip, 
    WorkoutsFilterMuscles, WorkoutsFilterLanguage } 
    from './Workouts/WorkoutsFilter';
import {WorkoutsSort} from './Workouts/WorkoutsSort';
import {HighlighterText} from './Highlight';
import Pagination from "react-pagination-bootstrap";

//all filter options for filtering by category
const category = [
    "Abs",
    "Arms",
    "Back",
    "Calves",
    "Chest",
    "Legs",
    "Shoulders",
]

//all filter options for filtering by equipment
const equipment = [
    "Barbell",
    "Bench",
    "Dumbell",
    "Gym mat",
    "Incline bench",
    "Kettlebell",
    "none (bodyweight exercise)",
    "Pull-up bar",
    "Swiss Ball",
    "SZ-Bar",
]

//all filter options for filtering by targeted muscles
const muscles = [
    "Anterior deltoid",
    "Biceps brachii",
    "Biceps femoris",
    "Brachialis",
    "Gastrocnemius",
    "Gluteus maximus",
    "Latissimus dorsi",
    "Obliquus externus abdominus",
    "Pectoralis major",
    "Quadriceps femoris",
    "Rectus abdominis",
    "Serratus anterior",
    "Soleus",
    "Trapezius",
    "Triceps brachii"
]

const languages = [
    "čeština",
    "Deutsch",
    "English",
    "Español",
    "Nederlands",
    "Norsk",
    "Português",
    "Svenska",
    "ελληνικά",
    "български език",
    "Русский"
]

export class Posts extends Component {
    state = {
        posts: [{
            id: "",
            name: "",
            description: "",
            category: "",
            muscles: "",
            secondary_muscle: "",
            equipment: "",
            image: "",
            license: "",
            license_author:"",
        }],
        loading: false, 
    };
    currentPage: number = 1;
    queryParams: {} = {};
    sortParams: string;
    filterCategory: string;
    filterValueCategory: string;
    filterEquip: string;
    filterEquipValue: string;
    filterMuscles: string;
    filterMusclesValue: string;
    stringQuery: string;
    filterLanguage: string;
    filterLanguageValue: string;

    constructor(props) {
        super(props);
        this.sortParams = "";
        this.filterCategory = "";
        this.filterValueCategory = "";
        this.filterEquip = "";
        this.filterEquipValue = "";
        this.filterMuscles = "";
        this.filterMusclesValue = "";
        this.stringQuery = "";
        this.filterLanguage = "";
        this.filterLanguageValue = "";
    }

    componentWillReceiveProps = (props) => {
        this.queryParams = props.queryParams;
        this.getPosts();
    }

    generateQueryStrings = () => {
        var result: string[] = [];
        for (var key in this.queryParams) {
            result.push(key.concat("=").concat(this.queryParams[key]));
        }
        return result.join("&");
    }

    createStringQuery = () => {
        var result: string = "";
        for (var key in this.queryParams) {
            result += (this.queryParams[key]);
        } 
        return result;
    }

    //pass in all the right filter, search, sort parameters to call our 
    //workouts api to obtain correct results
    getPosts = async () => {
        this.setState({loading: true});
        const pageString = '&page='.concat(this.currentPage.toString());
        const queryStrings = this.generateQueryStrings();
        this.stringQuery = this.createStringQuery();
        const searchUrl = 'https://api.healthathome.me/api/workouts?'
            .concat(queryStrings).concat("&order_by=")
            .concat(this.sortParams).concat("&ordering=asc").concat("&")
            .concat(this.filterCategory).concat("=")
            .concat(this.filterValueCategory).concat("&")
            .concat(this.filterEquip).concat("=")
            .concat(this.filterEquipValue).concat("&")
            .concat(this.filterMuscles).concat("=")
            .concat(this.filterMusclesValue)
            .concat("&").concat(this.filterLanguage)
            .concat("=").concat(this.filterLanguageValue)
            .concat(pageString);
            console.log(searchUrl);
        const results = await axios.get(searchUrl);
        console.log("AFter get call workouts " + searchUrl);
        this.setState({posts: results.data, loading: false});
    };

    componentDidMount() {
        this.getPosts();
    }

    //capture/update the sort field that the user chose
    handleSort = (e) => {
        this.sortParams = e.target.value.toString();
        this.getPosts();
    }

    //capture and update the filter field that the user 
    //chose to filter by category
    handleFilter = (e) => {
        //value of a filter
        this.filterValueCategory = e.target.value;
        for (var i = 0; i < category.length; i++){
            //obtain the category of that field
            if(this.filterValueCategory === category[i]){
                this.filterCategory = "category";
            }
        }
        this.getPosts();
    }

    //capture and update the filter field that the user 
    //chose to filter by equipment
    handleFilterEquip = (e) => {
        this.filterEquipValue = e.target.value;
        for (var j = 0; j < equipment.length; j++){
            if(this.filterEquipValue === equipment[j]){
                this.filterEquip = "equipment"
            }
        }
        this.getPosts();
    }

    //capture and update the filter field that the user 
    //chose to filter by muscle
    handleFilterMuscles = (e) => {
        this.filterMusclesValue = e.target.value;
        for (var j = 0; j < muscles.length; j++){
            if(this.filterMusclesValue === muscles[j]){
                this.filterMuscles = "muscles"
            }
        }
        this.getPosts();
    }

    handleFilterLanguage = (e) => {
        this.filterLanguageValue = e.target.value;
        for (var j = 0; j < languages.length; j++){
            if(this.filterLanguageValue === languages[j]){
                this.filterLanguage = "language"
            }
        }
        this.getPosts();
    }

    handlePageChange = (pageNumber) => {
        this.currentPage = pageNumber;
        this.getPosts();
    }


    render() {

        //display loading on website when page is loading
        if (this.state.loading) {
            return (
                <Row className="justify-content-center">
                    <Spinner animation="border" role="status"/>
                    <h4>Loading...</h4>
                </Row>
            )
        }

        return (
        <div>
        <WorkoutsSort handleSort = {this.handleSort} />
        <br></br>
        <WorkoutsFilter handleFilter = {this.handleFilter}/>
        <br></br>
        <WorkoutsFilterEquip handleFilterEquip = {this.handleFilterEquip}/>
        <br></br>
        <WorkoutsFilterMuscles handleFilterMuscles = 
            {this.handleFilterMuscles}/>
        <br></br>
        <WorkoutsFilterLanguage handleFilterLanguage= 
            {this.handleFilterLanguage}/>
        <br></br>
        {/* Display all correct instances */}
        <Row className="my-3">
            <Col xs={12} className = "text-center">
            </Col>
            {this.state.posts.map(post => (
            <Col lg={4} md={6} className="p-2"  key={post.name}>
                <Container className = 
                    "p-1 bg-light border border-dark rounded-lg">
                    {/* <h4>{post.name}</h4> */}
                    <h4><HighlighterText text = {post.name} 
                        searchValue={this.stringQuery}/></h4>
                    <h6>Category: <HighlighterText text = {post.category}
                        searchValue={this.stringQuery}/></h6>
                    <h6>Equipment: <HighlighterText text = {post.equipment}
                        searchValue={this.stringQuery}/></h6>
                    <h6>Muscles: <HighlighterText text = {post.muscles} 
                        searchValue={this.stringQuery}/></h6>
                    <Button href = {String("/workouts1/").concat(post.id)}>
                        Find out more</Button>

                </Container>
            </Col>
        ))}
        </Row>
            <Pagination
                prevPageText='prev'
                nextPageText='next'
                firstPageText='first'
                lastPageText='last'
                activePage={this.currentPage}
                itemsCountPerPage={10}
                totalItemsCount={200}
                pageRangeDisplayed={10}
                onChange={this.handlePageChange}
            />
        </div>
        )
    }
}
