import React from "react";
import Highlighter from "react-highlight-words";

//Component to highlight search results
function HighlighterText(props) {
    return (
      <Highlighter
        highlightStyle={{ padding: 0 }}
        searchWords={[props.searchValue]}
        autoEscape
        textToHighlight={props.text ? props.text.toString() : ""}
      />
    );
  }

export { HighlighterText};
