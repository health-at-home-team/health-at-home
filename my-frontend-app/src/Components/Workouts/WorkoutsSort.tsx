import React from 'react'
import { Form, Row, Col} from 'react-bootstrap'


//sorting component for workouts
export const WorkoutsSort = (props) => {
    return(
    <Row>
        <Col sm={5}>
            <Form>
                <Form.Label>Sort By</Form.Label>
                <Form.Control onChange={props.handleSort} as="select" 
                    type="submit">
                    <option value="">Select one</option>
                    <option value="name">Name</option>
                    <option value="category">Category</option>
                </Form.Control>
            </Form>
        </Col>
    </Row>
    )
}