import React from 'react'
import {Form, Row, Col} from 'react-bootstrap'


//filtering dropdown for categories
export const WorkoutsFilter = (props) => {
    return (
        <div>
        <Row>
            <Col sm={5}>
                <Form>
                    <Form.Label>Filter By Category</Form.Label>
                    <Form.Control onChange={props.handleFilter}  
                        type="submit" as = "select" >
                    <option value="">Category</option>
                    <option value = "Abs">Abs</option>
                    <option value = "Arms">Arms</option>
                    <option value = "Back">Back</option>
                    <option value = "Calves">Calves</option>
                    <option value = "Chest">Chest</option>
                    <option value = "Legs">Legs</option>
                    <option value = "Shoulders">Shoulders</option>
                    </Form.Control>
                </Form>
            </Col>
        </Row>
        </div>
    )
}

//dropdown to filter workouts by equipment
export const WorkoutsFilterEquip = (props) => {
    return(
    <Row>
        <Col sm={5}>
            <Form>
                <Form.Label>Filter By Equipment</Form.Label>
                <Form.Control onChange={props.handleFilterEquip}  
                    type="submit" as = "select" >
                <option value="" >Equipment</option>
                <option value = "Barbell">Barbell</option>
                <option value = "Bench">Bench</option>
                <option value = "Dumbbell">Dumbbell</option>
                <option value = "Gym mat">Gym Mat</option>
                <option value = "Incline bench">Incline Bench</option>
                <option value = "none (bodyweight exercise)">None</option>
                <option value = "Kettlebell">Kettlebell</option>
                <option value = "Pull-up bar">Pull-up bar</option>
                <option value = "Swiss Ball">Swiss Ball</option>
                <option value = "SZ-Bar">SZ-Bar</option>
                </Form.Control>
            </Form>
        </Col>
    </Row>
    )
}


//dropdown to filter workouts by equipment
export const WorkoutsFilterLanguage = (props) => {
    return(
    <Row>
        <Col sm={5}>
            <Form>
                <Form.Label>Filter By Language</Form.Label>
                <Form.Control onChange={props.handleFilterLanguage}  
                    type="submit" as = "select" >
                <option value="">Language</option>
                <option value = "English">English</option>
                <option value = "čeština">čeština</option>
                <option value = "Deutsch">Deutsch</option>
                <option value = "Español">Español</option>
                <option value = "Nederlands">Nederlands</option>
                <option value = "Norsk">Norsk</option>
                <option value = "Português">Português</option>
                <option value = "Svenska">Svenska</option>
                <option value = "ελληνικά">ελληνικά</option>
                <option value = "български език">български език</option>
                <option value = "Русский">Русский</option>
                </Form.Control>
            </Form>
        </Col>
    </Row>
    )
}


//dropdown to filter workouts by targeted muscles
export const WorkoutsFilterMuscles = (props) => {
    return(
    <Row>
        <Col sm={5}>
            <Form>
                <Form.Label>Filter By Targeted Muscles</Form.Label>
                <Form.Control onChange={props.handleFilterMuscles} 
                    type="submit" as = "select" >
                <option value="" >Muscles</option>
                <option value = "Anterior deltoid">Anterior deltoid</option>
                <option value = "Biceps brachii">Biceps brachii</option>
                <option value = "Biceps femoris">Biceps femoris</option>
                <option value = "Brachialis">Brachialis</option>
                <option value = "Gastrocnemius">Gastrocnemius</option>
                <option value = "Gluteus maximus">Gluteus maximus</option>
                <option value = "Latissimus dorsi">Latissimus dorsi</option>
                <option value = "Obliquus externus abdominus">Obliquus externus 
                    abdominus</option>
                <option value = "Pectoralis major">Pectoralis major</option>
                <option value = "Quadriceps femoris">Quadriceps femoris</option>
                <option value = "Rectus abdominis">Rectus abdominis</option>
                <option value = "Serratus anterior">Serratus anterior</option>
                <option value = "Soleus">Soleus</option>
                <option value = "Trapezius">Trapezius</option>
                <option value = "Triceps brachii">Triceps brachii</option>
                </Form.Control>
            </Form>
        </Col>
    </Row>
    )
}