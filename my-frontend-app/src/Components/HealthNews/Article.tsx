import React, {Component} from 'react'
import { Button, Image, Col, Container, Row } from 'react-bootstrap'
import axios from 'axios';
import { RouteComponentProps } from 'react-router-dom'

export class Article extends Component {
    state = {
        info: {
            title: "",
            author: "",
            publisher: "",
            date: "",
            image: "",
            url: "",
            description: "",
            content: ""
        }, 
        loading: false, 
    };

    id;

    constructor(props: RouteComponentProps<{id}> ) {
        super(props);
        const {id} = props.match.params;
        this.id = id;

    }

    componentDidMount() {

        const getArticle = async () => {
            
            this.setState({loading: true});
            const results = await axios.get
            ('https://api.healthathome.me'
            .concat('/api/healthnewsInfo?id=')
            .concat(this.id));

            const tempData = results.data;

            var articleInfo = {
                title: tempData.title,
                author: tempData.author,
                publisher: tempData.source,
                date: tempData.publishedAt,
                image: tempData.urlToImage,
                url: tempData.url,
                description: tempData.description,
                content: tempData.content,
            };
            this.setState({info: articleInfo, loading:false});
        };
        getArticle();
    }

    render() {
        const {info, loading} = this.state;

        return (
            <Container className="mb-5">
                <h3>{info.title}</h3>
                <small>{info.author}</small>
                <em className="mx-2">-</em>
                <small>{info.publisher}</small> <br/>
                <small>{info.date}</small>
                <hr/>
                <Row className="justify-content-center my-3">
                    <Col lg={6}>
                        <Image src={info.image} fluid />
                    </Col>
                </Row>

                <p>{info.description}</p>
                <p>{info.content}</p>
                <Button variant="Primary" 
                    className="bg-primary text-white" 
                    href={info.url}>View this article</Button>
                
            </Container>
        )
    }

}