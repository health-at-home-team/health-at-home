import React from 'react'
import {Button, Col, Form, InputGroup} from 'react-bootstrap'



export const HealthNewsSearch = (Props) => {

    var searchTerm;
    var searchKey;

    const textChangedFunc = (event) => {
        searchKey = event.target.name;
        searchTerm = event.target.value;
    }
    
    return (
        <Form onSubmit={Props.submitFunc}>
            <Form.Row className="py-1">
                <InputGroup>
                    <Form.Control onChange={Props.searchFunc} 
                        name="title" type="text" 
                        placeholder="Search for something here"/>
                    <InputGroup.Append>
                        <Button type="submit">Search</Button>
                    </InputGroup.Append>
                </InputGroup>
            </Form.Row>
            <Form.Row className="py-1">
                <InputGroup>
                    <Form.Control onChange={Props.searchFunc} 
                        name="source" type="text" 
                        placeholder="Filter by source"/>
                    <Form.Control onChange={Props.searchFunc} 
                        name="publishedAt" type="text" 
                        placeholder
                        ="Filter by date (must be exact, see instances)"/>
                </InputGroup>
            </Form.Row>
            <Form.Row className="justify-content-center py-1">
                <InputGroup as={Col}>
                    <Button>Order by:</Button>
                    <select onChange={Props.searchFunc} 
                        className="form-control" name="order_by" 
                        id="order_by">
                        <option>id</option>
                        <option>title</option>
                        <option>author</option>
                        <option>source</option>
                    </select>
                    <select onChange={Props.searchFunc} 
                        className="form-control" name="ordering" 
                        id="ordering">
                        <option>asc</option>
                        <option>desc</option>
                    </select>
                </InputGroup>
            </Form.Row> 
        </Form>
    )
}