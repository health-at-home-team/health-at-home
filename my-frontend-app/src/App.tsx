import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Home } from './Home'
import { About } from './About'
import { Recipes } from './Recipes1'
import { Workouts1 } from './Workouts'
import { HealthNews } from './HealthNews'
import { NoMatch } from './NoMatch'
import {Layout} from './Components/Layout'
import {NavigationBar} from './Components/NavigationBar'
import { Jumbotron } from './Components/Backgroundpic'
import {WorkoutInstance} from './WorkoutInstance'
import {Article} from "./Components/HealthNews/Article";
import {RecipeInstance} from './RecipeInstances'
import { SearchResults } from './SearchResults'
import { Visualizations } from "./Components/OurVisualizations";


function App() {

  return (
    <React.Fragment>
      <NavigationBar/>
      <Jumbotron/>
      <Layout>
        <Router>
          <Switch>
            <Route exact path ="/" component = {Home} />
            <Route path ="/about" component = {About} />
            <Route exact path ="/recipes" component = {Recipes} />
            <Route path ="/workouts" component = {Workouts1} />
            <Route exact path = "/workouts1/:id" 
              component = {WorkoutInstance} />
            <Route exact path ="/visualizations" component = {Visualizations}/>
            <Route exact path ="/recipes/:id" component = {RecipeInstance} />
            <Route exact path ="/healthnews" component = {HealthNews} />
            <Route exact path = "/healthnews/:id" component = {Article} />
            <Route exact path ="/search" component = {SearchResults} />
            <Route component = {NoMatch} />
          </Switch>
        </Router>
      </Layout>
    </React.Fragment>
  );
}

export default App;
