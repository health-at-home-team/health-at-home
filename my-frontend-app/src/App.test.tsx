import React from 'react';
import { render } from '@testing-library/react';
import {Home} from "./Home";
import {About} from "./About"
import {Recipes} from "./Recipes1";
import {Workouts} from "./Workouts";
import {HealthNews} from "./HealthNews";


// Testing Home Page renders
test("renders Home page", () => {
  const component = render(<Home />);
  const element = component.getByText("EMBRACE a healthy lifestyle today");
  expect(element).toBeInTheDocument();
});

// Testing About Page renders
test("renders About page", () => {
  const component = render(<About />);
  const element = component.getByText("Our Mission");
  expect(element).toBeInTheDocument();
});

// Testing Workouts models page renders
test("renders Workouts page", () => {
  const component = render(<Workouts />);
  const element = component.getByText("Workouts");
  expect(element).toBeInTheDocument();
});

// Testing Recipes models page renders
test("renders Recipes page", () => {
  const component = render(<Recipes />);
  const element = component.getByText("Recipes");
  expect(element).toBeInTheDocument();
});

// Testing Health News models page renders
 test("renders HealthNews page", () => {
  const component = render(<HealthNews />);
  const element = component.getByText("Health News");
  expect(element).toBeInTheDocument();
});

/*
// Testing HealthNews renders
test("renders HealthNews page", () => {
  const component = render(<HealthNews />);
  const element = component.getByText("Get the latest health news");
  expect(element).toBeInTheDocument();
});
*/
