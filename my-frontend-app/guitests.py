import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import time

# navigates through the site pages + makes basic use of functionality
# each test begins from homepage; navigate using navbar links
navbar = "//*[@id='root']/div[1]/nav" # make sure navbar is on every page

class SeleniumTests(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        # create a new Firefox session
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        # navigate to the application home page
        self.driver.get("http://localhost:80/")
        self.driver.title


    @classmethod
    def tearDownClass(self):
        self.driver.quit()


    # all tests start from homepage
    def setUp(self):
        self.driver.get("http://localhost:80/")


    def tearDown(self):
        print(f"\tCompleted: {self.id()}")


    def test_home_url(self):
        self.driver.find_element_by_xpath(navbar)


    # Site title in navbar should link to home
    def test_home_title(self):
        self.driver.find_element_by_link_text("Health At Home").click()
        self.driver.find_element_by_xpath(navbar)
        assert "Health" in self.driver.title
        assert "Health At Home" in self.driver.page_source
        assert "lifestyle" in self.driver.page_source
        assert "No Match" not in self.driver.page_source


    def test_home_home_link(self):
        self.driver.find_element_by_link_text("Home").click()
        self.driver.find_element_by_xpath(navbar)
        assert "Health" in self.driver.title
        assert "Health At Home" in self.driver.page_source
        assert "lifestyle" in self.driver.page_source
        assert "No Match" not in self.driver.page_source


    def test_recipes_basic(self):
        self.driver.find_element_by_link_text("Recipes").click()
        self.driver.find_element_by_xpath(navbar)
        assert "Recipes" in self.driver.page_source
        assert "Search" in self.driver.page_source
        assert "No Match" not in self.driver.page_source


    def test_recipes_search(self):
        self.driver.find_element_by_link_text("Recipes").click()
        # wait for initial load. can't find anything nicer to wait for
        time.sleep(2)
        elem = self.driver.find_element_by_name("title")
        elem.send_keys("curry mussels", Keys.RETURN)
        self.driver.find_element_by_link_text("Curry Mussels").click()

        # on some specific recipe's page
        self.driver.find_element_by_xpath(navbar)
        self.driver.find_element_by_xpath("//button[.='View this recipe!']")


    def test_workouts_basic(self):
        self.driver.find_element_by_link_text("Workouts").click()
        self.driver.find_element_by_xpath(navbar)
        assert "Workouts" in self.driver.page_source
        assert "Search" in self.driver.page_source
        assert "No Match" not in self.driver.page_source


    def test_workouts_search(self):
        self.driver.find_element_by_link_text("Workouts").click()
        # wait for initial load
        self.driver.find_element_by_link_text("Find out more")
        elem = self.driver.find_element_by_name("name")
        elem.send_keys("crunches", Keys.RETURN)
        self.driver.find_element_by_link_text("Find out more").click()

        # on some specific workout's page
        self.driver.find_element_by_xpath(navbar)


    def test_articles_basic(self):
        self.driver.find_element_by_link_text("Health News").click()
        self.driver.find_element_by_xpath(navbar)
        assert "Health News" in self.driver.page_source
        assert "Search" in self.driver.page_source
        assert "No Match" not in self.driver.page_source


    def test_articles_search(self):
        # wait for initial load
        self.driver.find_element_by_link_text("Health News").click()
        self.driver.find_element_by_link_text("Check out more...")
        elem = self.driver.find_element_by_name("title")
        elem.send_keys("chicken", Keys.RETURN)
        self.driver.find_element_by_link_text("Check out more...").click()

        # on some specific article's page
        self.driver.find_element_by_xpath(navbar)


    def test_about(self):
        self.driver.find_element_by_link_text("About").click()
        assert "Our Mission" in self.driver.page_source
        assert "Our Team" in self.driver.page_source
        assert "Tools and Documentation" in self.driver.page_source


if __name__ == "__main__":
    unittest.main()
