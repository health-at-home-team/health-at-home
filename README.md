# Health at Home

Health and diet based website to help people fulfill their health and wellness goals. This service was created as part of a series of projects for CS373 at the University of Texas at Austin.  

## Project Requirements
**Necessary information related to the grading of this project.**
* Git SHA: 9b51f6e66d0fb6a44a077c1982c7c9bd954c0b05
* Project Leader: Anwesha Roy
* Pipelines: https://gitlab.com/health-at-home-team/health-at-home/-/pipelines
* Website: https://www.healthathome.me
* Comments:

### Jonathan Serbent
* EID: jas24256
* GitLab ID:  jonathanserbent
* Estimated Completion Time: 10
* Actual Completion Time:  17

### Shruti Vellaturi
* EID: sv24865
* GitLab ID: shrutiv  
* Estimated Completion Time: 12 hours 
* Actual Completion Time:  12 hours

### Andrew Chen
* EID: atc2363
* GitLab ID: heisenbee13  
* Estimated Completion Time: 12 hours  
* Actual Completion Time: 13 hours

### Priya Patel
* EID: pkp493
* GitLab ID: priyapatel99  
* Estimated Completion Time: 8 hours  
* Actual Completion Time: 10 hours

### Anwesha Roy
* EID: ar62765
* GitLab ID: AnweshaRoy  
* Estimated Completion Time: 6 hours  
* Actual Completion Time: 10 hours

### Abdul-Ghaffar Balogun
* EID: gb23427
* GitLab ID: ghaffarbalogun
* Estimated Completion Time: 5 hours  
* Actual Completion Time:  3 hours

## Quickstart guide

### Installing the necessary tools

**Node and NPM**  
The first tool you will need to install is Node.js and Node Project Manager. [Follow this link to install Node.js and NPM](https://nodejs.org/en/download/).  

**Docker [not yet needed]**  
The next tool you will need is Docker, a tool used for creating virtual environments that can be deployed easily. [Follow this link to install Docker](https://docs.docker.com/get-docker/). It is important to note that if you are working on a Windows machine, you may have to take additional steps to set up virtualization. There are plenty of resources online to help you figure this out.

### Setting up the fork
To get started implementing changes to HealthAtHome.me, fork this repo, and clone the fork onto your development machine. `cd` into the project directory and then setup the remotes for the project. By default, `origin` will be set to your fork branch, but you can check this with the following command:  
```
.../health-at-home> git remote -v
origin https://gitlab.com/<your GitLab ID>/health-at-home.git (fetch)
origin https://gitlab.com/<your GitLab ID>/health-at-home.git (push)
```

You will want to rename that remote to your gitlab ID in order to avoid confusion, as well as adding the upstream remote. This can be done with the following commands:  
```
.../health-at-home> git remote add upstream https://gitlab.com/health-at-home-team/health-at-home.git
.../health-at-home> git remote rename origin <your GitLab ID>
.../health-at-home> git remote -v
<your GitLab ID> https://gitlab.com/<your GitLab ID>/health-at-home.git (fetch)
<your GitLab ID> https://gitlab.com/<your GitLab ID>/health-at-home.git (push)
upstream        https://gitlab.com/health-at-home-team/health-at-home.git (fetch)
upstream        https://gitlab.com/health-at-home-team/health-at-home.git (push)
```

Finally, you will want to set up your develop branch. To do this call the following:  
```
.../health-at-home> git checkout -b develop upstream/develop
```  

### Implementing Changes

For this project, all implementations start at the `develop` branch, get implemented into a new branch, accepted via a merge request, and then finally deployed when `develop` is merged into `master`. By doing this, we allow all of us to develop and make changes to the code effectively while reducing the number of conflicts and git issues.  
The first step of this process is to create a new branch from `develop`. We will first fetch any changes made in any of our remotes, then we will make sure `develop` is up to date, and finally we will `checkout` a new branch.  
```
.../health-at-home> git fetch --all
Fetching <your GitLab ID>
Fetching upstream
<if you have other remotes they will show up here...>
.../health-at-home> git checkout develop
Switched to branch 'develop'
.../health-at-home> git pull upstream develop
From https://gitlab.com/health-at-home-team/health-at-home
 * branch            develop    -> FETCH_HEAD
 .../health-at-home> git checkout -b <new branch>
```  

If you reach a point where you need resources that were merged in after you created your branch, you can easily catch up your branch by running `git pull upstream develop`.  
**It is very important to remember to have good commit titles and messages, and comment your code well so that it is easy to follow what changes you are making.** If possible also try to include the issue your implementations are related to by including `#issue-number` in either your commit messages or Merge Requests.  

## Building the Project

### Front-end
To run the front end locally when developing, simply navigate to the directory for the frontend app, and run the command to start: `npm start`.  
To build the front end, again navigate to the directory and run `npm run build`. This will generate the build files into the `build` directory withing the frontend app directory. This is also ran in AWS Amplify whenever a change is made to `develop` or `master` to allow for continuos deployment.
